# ris_project_node

Drone navigation through waypoints and pose based visual servoing (PBVS).

<p><img width="300" src="./assets/mission_ini.png" alt="drone_mission"></p>

## Build steps and running

1. Setup your environment as specified in: https://asantamarianavarro.gitlab.io/code/teaching/ris/ris_project_instructions/

    Note: you need UPC student credentials

2. Follow the steps:

```
cd <YOUR_PATH>/ris_project_ws/src
git clone https://gitlab.com/oriolvendrell/ris_project_node.git
cd ris_project_node/src
catkin build ris_project_node ris_project_node

roscd
cd ..
source devel/setup.bash
rosrun ris_project_node ris_project_node
```

    Note: you need to have the provided launch file running before starting this node

## Authors
Santi Prats, Diego Ruiz and Oriol Vendrell
