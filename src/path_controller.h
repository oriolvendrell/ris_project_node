#ifndef PATH_CONTROLLER_H_
#define PATH_CONTROLLER_H_

#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Pose.h"
#include <tf/transform_listener.h>
#include <tf/LinearMath/Vector3.h>
#include <tf/LinearMath/Quaternion.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <mavros_msgs/CommandTOL.h>
#include <cmath>

class PathController
{
    private:
      
      // node handle
      ros::NodeHandle n_;

      // Publishers
      ros::Publisher waypoint_pub_;
      ros::Publisher vis_pub_;
    
      // Subscribers
      ros::Subscriber pose_sub_;
      ros::Subscriber aruco_sub_;

      // Transform transform_listener
      tf::TransformListener* tfListener_;

      // Landing service
      ros::ServiceClient land_client_;
      
      // atributes
      bool visualservoing_;
      double margin_; // margin in front of the aruco wall

      geometry_msgs::PoseStamped waypoint_;  // waypoint navigation goal
      std::vector<geometry_msgs::Pose> waypoints_path_; // waypoints path to the aruco
      int current_waypoint_; // waypoints path current index

      visualization_msgs::MarkerArray marker_a_;

      int count_;

      bool landing_;
      geometry_msgs::PoseStamped desiredpose_; // Final desired position

      // methods
      bool equalPose(geometry_msgs::Pose waypoint, geometry_msgs::Pose local_pose);
   
    public:

      // callbacks
      void poseCheckCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
      void arucoCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
      
      // public methods
      void publishWaypoint();

      // constructor
      PathController(int argc, char**argv);
   
};

#endif
