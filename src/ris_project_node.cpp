#include "path_controller.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "publish_path");
 
  PathController path(argc, argv);

  // Rate to keep publishing the waypoint for the drone navigation
  // The callbacks in the path controller are responsible for updating the waypoint
  ros::Rate loop_rate(50);
  while (ros::ok())
  {
    path.publishWaypoint();

    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
