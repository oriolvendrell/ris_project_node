#include "path_controller.h"

PathController::PathController(int argc, char**argv)
{
    // init atributes
    visualservoing_ = false;
    current_waypoint_ = 0;
    margin_ = 1.0;
    landing_ = false;


    // init final position
    desiredpose_.header.seq = 0;
    desiredpose_.header.stamp = ros::Time::now();
    desiredpose_.header.frame_id = "map";

    desiredpose_.pose.position.x = 0.0;
    desiredpose_.pose.position.y = 0.0;
    desiredpose_.pose.position.z = 0.0;
    desiredpose_.pose.orientation.x = 0.0;
    desiredpose_.pose.orientation.y = 0.0;
    desiredpose_.pose.orientation.z = 1.0;
    desiredpose_.pose.orientation.w = 0.0;

    // Waypoint publisher
    waypoint_pub_ = n_.advertise<geometry_msgs::PoseStamped>("/uav1/mavros/setpoint_position/local", 1000);
    // Visualization publisher
    vis_pub_ = n_.advertise<visualization_msgs::MarkerArray>("visualization_marker", 1000);
    // Pose subscriber
    pose_sub_ = n_.subscribe("/uav1/mavros/local_position/pose", 1000, &PathController::poseCheckCallback, this);
    // Aruco subscriber
    aruco_sub_ = n_.subscribe("/uav1/aruco_single/pose",1000, &PathController::arucoCallback, this);

    // Transform listener
    tfListener_ = new tf::TransformListener;

    // Landing service
    land_client_ = n_.serviceClient<mavros_msgs::CommandTOL>("/uav1/mavros/cmd/land");

    // List of waypoints to arrive in front of to the aruco marker
    geometry_msgs::Pose pose;
    pose.position.x = 0.0;
    pose.position.y = 0.0;
    pose.position.z = 12.0;
    pose.orientation.x = 0.0;
    pose.orientation.y = 0.0;
    pose.orientation.z = 1.0;
    pose.orientation.w = 0.0;

    waypoints_path_.push_back(pose);

    pose.position.x = -15.0;
    pose.position.y = 0.0;
    pose.position.z = 8.0;
    pose.orientation.x = 0.0;
    pose.orientation.y = 0.0;
    pose.orientation.z = 1.0;
    pose.orientation.w = 0.0;

    waypoints_path_.push_back(pose);

    pose.position.x = -20.0;
    pose.position.y = 1.0;
    pose.position.z = 6.7;
    pose.orientation.x = 0.0;
    pose.orientation.y = 0.0;
    pose.orientation.z = 1.0;
    pose.orientation.w = 0.0;

    waypoints_path_.push_back(pose);

    pose.position.x = -30.6;
    pose.position.y = 1.0;
    pose.position.z = 6.7;
    pose.orientation.x = 0.0;
    pose.orientation.y = 0.0;
    pose.orientation.z = 1.0;
    pose.orientation.w = 0.0;

    waypoints_path_.push_back(pose);

    // Set first waypoint to start moving
    waypoint_.header.seq = 0;
    waypoint_.header.stamp = ros::Time::now();
    waypoint_.header.frame_id = "map";
   
    waypoint_.pose = waypoints_path_[current_waypoint_];
    ROS_INFO("Started WAYPOINT navigation mode");

};

void PathController::poseCheckCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    // Navigate through waypoints until we capture the aruco marker
    if (equalPose(waypoint_.pose, msg->pose) && !visualservoing_ && !landing_) {
        if (current_waypoint_ < 3) {
          current_waypoint_++;
          ROS_INFO("Waypoint: %d", current_waypoint_);
          waypoint_.pose = waypoints_path_[current_waypoint_];
        } else {
            visualservoing_ = true;
            ROS_INFO("Started VISUAL SERVOING navigation mode");
        }
    }

    if(equalPose(desiredpose_.pose, msg->pose) && visualservoing_ && !landing_) {
        landing_ = true;
        visualservoing_ = false;
        mavros_msgs::CommandTOL land_cmd;
        land_cmd.request.altitude = 0;
        land_client_.call(land_cmd);
    }

    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time();
    marker.id = count_;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose = msg->pose;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    if (visualservoing_) {
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
    } else if (landing_) {
        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;
    } else {
        marker.color.r = 1.0;
        marker.color.g = 0.0;
        marker.color.b = 0.0;
    }
    marker.lifetime = ros::Duration();

    marker_a_.markers.push_back(marker);
    vis_pub_.publish(marker_a_);
    count_++;

};

void PathController::arucoCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    if (!visualservoing_ || landing_) {
        return;
    }
    // get camera_Tf_aruco
    tf::Transform camera_Tf_aruco;
    camera_Tf_aruco.setOrigin(tf::Vector3(msg->pose.position.x, msg->pose.position.y, msg->pose.position.z));
    camera_Tf_aruco.setRotation(tf::Quaternion(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z, msg->pose.orientation.w));

    // set desired offset desiredCamera_Tf_aruco;
    tf::Transform desiredCamera_Tf_aruco;
    desiredCamera_Tf_aruco.setOrigin(tf::Vector3(msg->pose.position.x, msg->pose.position.y, msg->pose.position.z + margin_));
    camera_Tf_aruco.setRotation(tf::Quaternion(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z, msg->pose.orientation.w));

    // get map_Tf_camera
    tf::StampedTransform map_STf_camera;
    tfListener_->lookupTransform("map", "uav1/d435_front/color_optical_frame", ros::Time(0), map_STf_camera);
    tf::Transform map_Tf_camera(map_STf_camera.getBasis(), map_STf_camera.getOrigin());

    // get baselink_Tf_camera
    tf::StampedTransform baselink_STf_camera;
    tfListener_->lookupTransform("uav1/base_link", "uav1/d435_front/color_optical_frame", ros::Time(0), baselink_STf_camera);
    tf::Transform baselink_Tf_camera(baselink_STf_camera.getBasis(), baselink_STf_camera.getOrigin());

    // compose transforms to get map_Tf_baselink
    tf::Transform map_Tf_baselink = map_Tf_camera * camera_Tf_aruco * desiredCamera_Tf_aruco.inverse() * baselink_Tf_camera.inverse();

    // publish resulting waypoint
    waypoint_.pose.position.x = map_Tf_baselink.getOrigin().getX();
    waypoint_.pose.position.y = map_Tf_baselink.getOrigin().getY();
    waypoint_.pose.position.z = map_Tf_baselink.getOrigin().getZ();
    waypoint_.pose.orientation.x = map_Tf_baselink.getRotation().x();
    waypoint_.pose.orientation.y = map_Tf_baselink.getRotation().y();
    waypoint_.pose.orientation.z = map_Tf_baselink.getRotation().z();
    waypoint_.pose.orientation.w = map_Tf_baselink.getRotation().w();
    
    desiredpose_ = waypoint_;
    
};

void PathController::publishWaypoint()
{
    waypoint_pub_.publish(waypoint_);
};

bool PathController::equalPose(geometry_msgs::Pose waypoint_pose, geometry_msgs::Pose local_pose)
{
    float tol = 0.55;

    if(std::abs(waypoint_pose.position.x - local_pose.position.x) < tol &&
    std::abs(waypoint_pose.position.y - local_pose.position.y) < tol &&
    std::abs(waypoint_pose.position.z - local_pose.position.z) < tol) {
        return true;
    }
    return false;
};
